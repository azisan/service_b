# Service B merupakan REST API yang dibuat menggunakan Flask framework, JWT, SQLAlchemy.
Terdapat fitur login, register, read, update, delete, dan transaksi yang dilakukan terhadap Service A.
Dibutuhkan Service A supaya service ini dapat berjalan baik. Service B ini dapat berjalan secara automatis 
melakukan transaksi pada Service A yaitu melalui Endpoint pada service A yang sudah disediakan.

## Instalasi

1. Kloning repositori `$ git clone git@bitbucket.org:azisan/service_b.git`
2. Buat sebuah virtualenv untuk mengisolasi python packages dengan cara `$ virtualenv myenv`
3. Aktifkan virtualenv `$ source myenv/bin/activate`
4. Install beberapa package pendukung `$ pip install -r requirements.txt`
5. Sediakan sebuah database postgresql dengan nama misalnya `service_b` 
6. Konfigurasi database pada app.py line 22 `app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://username:password@host/service_a'`
7. Membuat tabel User pada database dengan cara masuk ke dalam python shell `$ python` kemudian `>>> from app import db` `>>> db.create_all()`
8. Uncomment baris code ke 177 supaya timer dapat berjalan
9. Jalankan server `$ python app.py`

## Berikut adalah beberapa endpoint yang tersedia:
- `GET` http://127.0.0.1:5001/transaksi untuk melihat semua transaksi yang sudah terjadi
- `GET` http://127.0.0.1:5001/transaksi`<id>` untuk melihat satu buah transaksi yang sudah terjadi
- `PUT` http://127.0.0.1:5001/transaksi`<id>` untuk mengupdate sebuah transaksi
- `DELETE` http://127.0.0.1:5001/transaksi`<id>`untuk menghapus sebuah transaksi

- `POST` http://127.0.0.1:5001/register untuk registrasi user
`payload {"username": "example", "password": "mysecret", "saldo": 9999}`
- `POST` http://127.0.0.1:5001/login untuk login dan mendapatkan token
`payload {"username": "example", "password": "mysecret"}`
- `GET` http://127.0.0.1:5001/users untuk menampilkan semua user yang ada
- `GET` http://127.0.0.1:5001/user/`<id>` untuk menampilkan satu user
- `PUT` http://127.0.0.1:5001/user/`<id>` untuk mengupdate user
`payload {"username": "example", "password": "mysecret", "saldo": 9999}`
- `DELETE` http://127.0.0.1:5001/user/`<id>` untuk menghapus user
- `POST` http://127.0.0.1:5001/transaksi untuk Service B melakukan transaksi