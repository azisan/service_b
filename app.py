from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.sql import func

from apscheduler.schedulers.background import BackgroundScheduler
import requests
import json

#Init app
app = Flask(__name__)

app.config['JWT_SECRET_KEY'] = 'Jy0a34DF$$Tb7WhbZ68KRxZuG'
jwt = JWTManager(app)

#Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:example@172.25.0.5/service_b'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#Init db
db = SQLAlchemy(app)

#Init marshmallow
ma = Marshmallow(app)


#MODEL Transaksi
class Transaksi(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.String(100))
    amount = db.Column(db.Integer)
    trx_at = db.Column(db.DateTime(timezone=True), server_default=func.now())

    def __init__(self, user_id, amount):
        self.user_id = user_id
        self.amount = amount
        
    
#Transaksi Schema
class TransaksiSchema(ma.Schema):
    class Meta:
        fields = ('id', 'user_id', 'amount', 'trx_at')

#Init schema
tran_schema = TransaksiSchema()
trans_schema = TransaksiSchema(many=True)

#Get All Transaksi
@app.route('/transaksi', methods=['GET'])
@jwt_required
def trans():
    trans = Transaksi.query.all()
    result = trans_schema.dump(trans)
    return jsonify(result)

#Get a Transaksi
@app.route('/transaksi/<id>', methods=['GET'])
@jwt_required
def get_transaksi(id):
    transaksi = Transaksi.query.get(id)
    return tran_schema.jsonify(transaksi)

#Update a Transaksi
@app.route('/transaksi/<id>', methods=['PUT'])
@jwt_required
def update_transaksi(id):
    transaksi = Transaksi.query.get(id)
    transaksi.amount = request.json['amount']
    db.session.commit()
    return tran_schema.jsonify(transaksi)

#Delete a User
@app.route('/transaksi/<id>', methods=['DELETE'])
@jwt_required
def delete_transaksi(id):
    transaksi = Transaksi.query.get(id)
    db.session.delete(transaksi)
    db.session.commit()
    return tran_schema.jsonify(transaksi)

#MODEL User service b
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(200))
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())
    
    def __init__(self, username, password):
        self.username = username
        self.password = password
        
    def check_password(self, password):
        """Check hashed password."""
        return check_password_hash(self.password, password)

#User Schema
class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'username', 'password', 'created_at')

#Init schema
user_schema = UserSchema()
users_schema = UserSchema(many=True)

#Register
@app.route('/register', methods=['POST'])
def register():
    username = request.json['username']
    password = request.json['password']
    user = User.query.filter_by(username=username).first() 
    if user: 
        return jsonify({"msg": "username exists!"})
    new_user = User(username, generate_password_hash(password, method='sha256'))
    db.session.add(new_user)
    db.session.commit()
    return user_schema.jsonify(new_user)

#Login
@app.route('/login', methods=['POST'])
def login():
    username = request.json['username']
    password = request.json['password']
    user = User.query.filter_by(username=username).first() 
    if user and user.check_password(password=password):
        access_token = create_access_token(identity=username)
        return jsonify(access_token=access_token), 200
    return jsonify({"msg": "Not login"})

#Get a User
@app.route('/user/<id>', methods=['GET'])
@jwt_required
def get_user(id):
    user = User.query.get(id)
    return user_schema.jsonify(user)

#Update a User
@app.route('/user/<id>', methods=['PUT'])
@jwt_required
def update_user(id):
    user = User.query.get(id)
    user.username = request.json['username']
    user.password = request.json['password']
    db.session.commit()
    return user_schema.jsonify(user)

#Delete a User
@app.route('/user/<id>', methods=['DELETE'])
@jwt_required
def delete_user(id):
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()
    return user_schema.jsonify(user)

#Scheduler
def post_transaksi():
    init_amount = 5
    init_headers = {'Content-Type': 'application/json',}
    init_url = 'http://127.0.0.1:5001/transaksi'
    transaksi = requests.post(init_url, headers=init_headers, json={"amount": init_amount}, verify=False)
    json_response = transaksi.json()
    for val in json_response:
        user_id = str(val['id'])
        amount = init_amount
        new_transaksi = Transaksi(user_id, amount)
        db.session.add(new_transaksi)
        db.session.commit()

scheduler = BackgroundScheduler(daemon=True)
scheduler.add_job(post_transaksi, 'interval', seconds=5)

# scheduler.start()
print("Job Started")
    
if __name__ == '__main__':
    app.env="development"
    app.run(host="127.0.0.1", port=5000, debug=True, use_reloader=False)
    